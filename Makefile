# ********************************************************************
# Argonone Applet Makefile
# ********************************************************************
CC      = gcc
RM      = rm -v
INSTALL = install
ARCH	= $(shell getconf LONG_BIT)
GTKLIB	= $(shell pkg-config --list-all | sed -ne 's/\(gtk+-[0-9]*.0\).*/\1/p')
GTKVER  = $(shell pkg-config --list-all | sed -ne 's/gtk+-\([0-9]*\).0.*/\1/p')

argonone.so: main.c
	$(CC) -Wall `pkg-config --cflags $(GTKLIB) lxpanel` -shared -fPIC main.c -o argonone.so `pkg-config --libs lxpanel` -DGTK=$(GTKVER)
	
# window_test: window.c
#	$(CC) -Wall  window.c -o window_test `pkg-config --libs --cflags $(GTKVER)` -lrt -DSTAND_ALONE

all: argonone.so

install-plugin: argonone.so
	$(INSTALL) argonone.so /usr/lib/*/lxpanel/plugins	
	lxpanelctl restart

install-icons:
	$(INSTALL) icons/48x48/argonone-fan* /usr/share/icons/hicolor/48x48/status/
	$(INSTALL) icons/16x16/argonone-fan* /usr/share/icons/hicolor/16x16/status/
	gtk-update-icon-cache -f /usr/share/icons/hicolor/

install: install-plugin install-icons
	@echo "Install Complete"

	
uninstall:
	$(RM) /usr/lib/*/lxpanel/plugins/argonone.so
	$(RM) /usr/share/icons/hicolor/48x48/status/argonone-fan*
	gtk-update-icon-cache -f /usr/share/icons/hicolor/
	lxpanelctl restart

clean:
	$(RM) argonone.so