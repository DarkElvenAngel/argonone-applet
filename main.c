/*
MIT License

Copyright (c) 2020 DarkElvenAngel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <lxpanel/plugin.h>
#include <lxpanel/conf.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "../argononed/src/argononed.h"

#define INTV 1
#define VERSION "0.0.5"
#ifndef LOG_LEVEL
#define LOG_LEVEL 6
#endif
#ifndef GTK
#error GTK is not set
#endif

#define VALUE_TO_STRING(x) #x
#define VALUE(x) VALUE_TO_STRING(x)
#define VAR_NAME_VALUE(var) #var "="  VALUE(var)
#pragma message VAR_NAME_VALUE(GTK)
#pragma message VAR_NAME_VALUE(LOG_LEVEL)


GtkWidget *pLabel; 
gchar *prstr;


typedef struct 
{
  LXPanel *panel;
  config_setting_t *settings;

  GtkWidget *plugin;
  GtkWidget *tray_icon;
  GtkWidget *tray_label;
  GtkWidget *popup_menu;
  

  // Dialog window
  gboolean show_state;
  gboolean show_speed;
  gboolean show_temp;
  int cooldown_temp;
  int cooldown_fan;

  int manual_fan;
  int target_mode;
  int current_mode;

  GtkWidget *window;
  struct DTBO_Config Schedule;

} TestPlugin;

gboolean getstate(gpointer data)
{
    TestPlugin *pTest = (TestPlugin *)data;
    struct SHM_Data* ptr;
    gchar *icon_name = NULL;
    gchar *mode_text = NULL;
    gchar *fan_text = NULL;
    int shm_fd =  shm_open(SHM_FILE, O_RDWR, 0664);
    if (shm_fd == -1)
    {
      return TRUE;
    }
    if (ftruncate(shm_fd, SHM_SIZE) == -1)
    {
      close(shm_fd);
      return TRUE;
    }
    ptr = mmap(0, SHM_SIZE, PROT_READ| PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (ptr == MAP_FAILED) {
        // Maybe the daemon is offline
        icon_name = "argonone-fan-error";
        if (pTest->show_state | pTest->show_speed | pTest-> show_temp)
        {
          prstr = g_strdup_printf("Offline");
        }
        else
        {
          prstr = g_strdup_printf('\0');
        }
        
        mode_text = "Unknown";
        fan_text = "---";
        goto CLEANUP;
    }
    prstr = g_strdup_printf("%s%s%s%s",
        pTest->show_state ? g_strdup_printf("%3s ", (ptr->fanspeed == 0x00 ? "OFF" : "ON")) : "",
        pTest->show_speed ? g_strdup_printf("%3d%%", (ptr->fanspeed)) : "",
        pTest->show_speed & pTest->show_temp ? " | " : "",
        pTest->show_temp  ? g_strdup_printf("%2d°", (ptr->temperature)) : ""
      );
    switch (ptr->fanmode)
    {
    case 0: // Auto
      icon_name = "argonone-fan-auto";
      mode_text = "automatic";
      break;
    case 1: // Off
      icon_name = "argonone-fan-off";
      mode_text = "off";
      break;
    case 2: // Manual
      icon_name = "argonone-fan-man";
      mode_text = "manual";
      break;
    case 3: // Cool Down
      icon_name = "argonone-fan-cooldown";
      mode_text = g_strdup_printf("cool-down\nTarget: %2d°",ptr->temperature_target);
      break;
    default: // never should happen ERROR
      icon_name = "argonone-fan-error";
      mode_text = "ERROR";
      break;
    }
    pTest->current_mode=ptr->fanmode;
    fan_text = g_strdup_printf("%s%s",
      ptr->fanspeed == 0x00 ? "off" : "on",
      ptr->fanspeed != 0x00 ? g_strdup_printf(" @ %d%%", ptr->fanspeed) : ""
    );
    munmap(ptr,SHM_SIZE);
CLEANUP: 
  close(shm_fd);
  gtk_label_set_text(GTK_LABEL(pTest->tray_label), prstr);
  gtk_widget_set_tooltip_text(pTest->plugin, g_strdup_printf("ArgonOne Fan Status\nMode: %s\nFan: %s",
      mode_text,
      fan_text
  ));
  lxpanel_plugin_set_taskbar_icon(
      pTest->panel, pTest->tray_icon,
      icon_name);
  g_free(prstr);
  return TRUE; 
}
// --------------------------------------------------------------------------------
// Command STUFF
static void EXECUTE_COMMAND(TestPlugin *pTest)
{
  char command[64] = {'\0'}; 

  switch (pTest->target_mode)
  {
  case 0:
    sprintf(command, "/usr/bin/argonone-cli -a");
    break;
  case 1:
    sprintf(command, "/usr/bin/argonone-cli -o");
    break;
  case 2:
    sprintf(command,"/usr/bin/argonone-cli -m -f%d", pTest->manual_fan);
    break;
  case 3:
    sprintf(command, "/usr/bin/argonone-cli -c%d -f%d",pTest->cooldown_temp, pTest->cooldown_fan);
    break;
  default:
    goto CLEANUP;
  }
  system(command);
CLEANUP:
  pTest->target_mode=-1;
}

static void test_mnu_auto(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode == 0) | (pTest->current_mode == -1)) return;
  pTest->target_mode = 0;
  pTest->manual_fan = 0;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_off(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode == 1) | (pTest->current_mode == -1)) return;
  pTest->target_mode = 1;
  pTest->manual_fan = 0;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_man(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode == 2) | (pTest->current_mode == -1)) return;
  pTest->target_mode = 2;
  pTest->manual_fan = 0;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_cool(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode == 3) | (pTest->current_mode == -1)) return;
  pTest->target_mode = 3;
  pTest->manual_fan = 0;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_up(GtkWidget *widget, TestPlugin *pTest)
{
  if (/*(pTest->current_mode != 2) | */(pTest->manual_fan >= 100 )) return;
  pTest->target_mode = 2;
  pTest->manual_fan += 10;
  EXECUTE_COMMAND(pTest);
}
static void test_mnu_down(GtkWidget *widget, TestPlugin *pTest)
{
  if ((pTest->current_mode != 2) | (pTest->manual_fan <= 0) ) return;
  pTest->target_mode = 2;
  pTest->manual_fan -= 10;
  EXECUTE_COMMAND(pTest);
}

static void test_mnu_about(GtkWidget *widget, TestPlugin *pTest)
{
  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file("/usr/share/icons/hicolor/48x48/status/argonone-fan-auto.png", NULL);

  GtkWidget *dialog = gtk_about_dialog_new();
#if GTK == 2
  gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(dialog), "Argon One Applet");
#elif GTK == 3
  gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Argon One Applet");
#else

#endif
  gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(dialog), VERSION); 
  gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(dialog),"(c) DarkElvenAngel");
  gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), 
     "Argon one daemon applet for LXPanel.");
  gtk_about_dialog_set_website(GTK_ABOUT_DIALOG(dialog), 
     "https://gitlab.com/DarkElvenAngel/argonone-applet");
  gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG(dialog), pixbuf);
  g_object_unref(pixbuf), pixbuf = NULL;
  gtk_dialog_run(GTK_DIALOG (dialog));
  gtk_widget_destroy(dialog);
}

void Spun (GtkSpinButton *spinbutton, uint8_t* temp)
{
  // int* temp = (int*)user_data;
  *temp =  gtk_spin_button_get_value (spinbutton);
}
void Click_Cancel(GtkWidget *Button, TestPlugin *pTest)
{
  // DISTROY Window
  gtk_widget_destroy(pTest->window);
}
void Click_Commit(GtkWidget *Button, TestPlugin *pTest)
{
  struct SHM_Data* ptr;
  int shm_fd =  shm_open(SHM_FILE, O_RDWR, 0664);
    if (shm_fd == -1)
    {
      // log_message(LOG_DEBUG, "failed to on shm_open");
      return;
    }
    if (ftruncate(shm_fd, SHM_SIZE) == -1)
    {
      // log_message(LOG_DEBUG, "failed to ftruncate");
      close(shm_fd);
      return;
    }
    ptr = mmap(0, SHM_SIZE, PROT_READ| PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (ptr == MAP_FAILED) {
      // log_message(LOG_DEBUG, "failed to mmap");
      close(shm_fd);
      return;
    }
  uint8_t last_state = 0;
  if (ptr->status == REQ_WAIT) 
  {
    // log_message(LOG_DEBUG, "Preping request");
    memcpy(&ptr->config, &pTest->Schedule, sizeof(struct DTBO_Config));

    // log_message(LOG_DEBUG,"Hysteresis set to %d°",ptr->config.hysteresis);
    // log_message(LOG_DEBUG,"Fan Speeds set to %d%% %d%% %d%%",ptr->config.fanstages[0],ptr->config.fanstages[1],ptr->config.fanstages[2]);
    // log_message(LOG_DEBUG,"Fan Temps set to %d° %d° %d°",ptr->config.thresholds[0],ptr->config.thresholds[1],ptr->config.thresholds[2]);
    ptr->status = REQ_RDY;
    for(;ptr->status != REQ_WAIT;) 
    {
      if (last_state != ptr->status)
      {
        last_state = ptr->status;
        if (ptr->status == REQ_ERR)
        {
          // log_message(LOG_ERROR, "SOMETHING WENT WRONG!");
          break;
        }
      }
      msync(ptr,13,MS_SYNC);
    } 
  }
  ptr->status = REQ_CLR;
  // log_message(LOG_DEBUG, "request complete");
  close(shm_fd);
  gtk_widget_destroy(pTest->window);
  // log_message(LOG_DEBUG, "window destroy");
  //gtk_main_quit();
}

GdkPixbuf *create_pixbuf(const gchar * filename) {
    
   GdkPixbuf *pixbuf;
   GError *error = NULL;
   pixbuf = gdk_pixbuf_new_from_file(filename, &error);
   
   if (!pixbuf) {
       
      fprintf(stderr, "%s\n", error->message);
      g_error_free(error);
   }

   return pixbuf;
}

static void test_mnu_schedules(GtkWidget *widget, TestPlugin *pTest)
{
  GtkWidget *window;

  GdkPixbuf *icon;

  GtkWidget *table;
  GtkWidget *title[2];
  GtkWidget *label[4];
  
  GtkWidget *hbox;
  //GtkWidget *halign;
  //GtkWidget *halign2;
  //GtkWidget *valign;

  GtkWidget *fanSpin[3];
  GtkWidget *tempSpin[3];
  GtkWidget *hystSpin;
  GtkWidget *warnIcon[3];

  GtkWidget *actBtn;
  GtkWidget *canBtn;
//  { {10, 55, 100}, {55, 60, 65}, 10 };

  struct SHM_Data* ptr;
  int shm_fd =  shm_open(SHM_FILE, O_RDWR, 0664);
    if (shm_fd == -1)
    {
      // log_message(LOG_DEBUG, "failed to on shm_open");
      return;
    }
    if (ftruncate(shm_fd, SHM_SIZE) == -1)
    {
      // log_message(LOG_DEBUG, "failed to ftruncate");
      close(shm_fd);
      return;
    }
    ptr = mmap(0, SHM_SIZE, PROT_READ| PROT_WRITE, MAP_SHARED, shm_fd, 0);
    if (ptr == MAP_FAILED) {
      close(shm_fd);
      return;
    }
  pTest->Schedule = ptr->config;
  
  pTest->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  window = pTest->window;
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
  gtk_widget_set_size_request (window, 300, 250);
  
  gtk_window_set_title(GTK_WINDOW(window), "Fan Setting");

  gtk_container_set_border_width(GTK_CONTAINER(window), 15);
  
  icon = create_pixbuf("/usr/share/icons/hicolor/48x48/status/argonone-fan.png");  
  gtk_window_set_icon(GTK_WINDOW(window), icon);
  
  table = gtk_table_new(7, 4, FALSE);
  gtk_table_set_col_spacings(GTK_TABLE(table), 3);
  gtk_table_set_row_spacing(GTK_TABLE(table), 0, 3);
  
  // label full width
  title[0] = gtk_label_new("Trigger Temp");
  title[1] = gtk_label_new("Fan Speed");
  //halign = gtk_alignment_new(0, 0, 0, 0);
  //gtk_container_add(GTK_CONTAINER(halign), title);
  //gtk_table_attach(GTK_TABLE(table), halign, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
  gtk_table_attach_defaults(GTK_TABLE(table), title[0], 1, 2, 0, 1);
  gtk_table_attach_defaults(GTK_TABLE(table), title[1], 2, 3, 0, 1);
  
  for (int i = 0;i < 3; i++)
  {
    label[i]    = gtk_label_new( g_strdup_printf("Schedule %d", i + 1) );
    tempSpin[i] = gtk_spin_button_new (GTK_ADJUSTMENT(gtk_adjustment_new (pTest->Schedule.thresholds[i],//current value
                    30, 
                    80,
                    1,
                    1,
                    0)), 1, 0);
    g_signal_connect(G_OBJECT(tempSpin[i]), "value-changed", G_CALLBACK(Spun), &pTest->Schedule.thresholds[i]);
    fanSpin[i] = gtk_spin_button_new (GTK_ADJUSTMENT(gtk_adjustment_new (pTest->Schedule.fanstages[i], //current value
                    0,
                    100, 
                    1,
                    1,
                    0)), 1, 0);
    g_signal_connect(G_OBJECT(fanSpin[i]), "value-changed", G_CALLBACK(Spun), &pTest->Schedule.fanstages[i]);
    
    warnIcon[i] = gtk_image_new_from_stock(0, GTK_ICON_SIZE_MENU);
    
    gtk_table_attach_defaults(GTK_TABLE(table), label[i]   , 0, 1, i+1, i+2);
    gtk_table_attach_defaults(GTK_TABLE(table), tempSpin[i], 1, 2, i+1, i+2);
    gtk_table_attach_defaults(GTK_TABLE(table), fanSpin[i] , 2, 3, i+1, i+2);
    gtk_table_attach_defaults(GTK_TABLE(table), warnIcon[i] , 3, 4, i+1, i+2);
  }
  label[4] = gtk_label_new("Hysteresis");
  hystSpin = gtk_spin_button_new (GTK_ADJUSTMENT(gtk_adjustment_new (pTest->Schedule.hysteresis,
                    0, //current value
                    10,
                    1,
                    1,
                    0)), 1, 0);//gtk_button_new_with_label("Activate");
    g_signal_connect(G_OBJECT(hystSpin), "value-changed", G_CALLBACK(Spun), &pTest->Schedule.hysteresis);
    gtk_table_attach_defaults(GTK_TABLE(table), label[4], 0, 2, 5, 6);
    gtk_table_attach_defaults(GTK_TABLE(table), hystSpin, 2, 3, 5, 6);
#if GTK == 2
  hbox = gtk_hbox_new(TRUE, 3);
#elif GTK == 3
  hbox = gtk_box_new(TRUE, 3);
#else

#endif
  canBtn = gtk_button_new_with_label("Cancel");
  actBtn = gtk_button_new_with_label("Commit");
  gtk_widget_set_size_request(canBtn, 70, 30);
  gtk_widget_set_size_request(actBtn, 70, 30);
  gtk_container_add(GTK_CONTAINER(hbox), canBtn);
  gtk_container_add(GTK_CONTAINER(hbox), actBtn);
  gtk_table_attach(GTK_TABLE(table), hbox, 0, 4, 7, 8, GTK_FILL, GTK_FILL, 0, 0);
  //gtk_table_attach(GTK_TABLE(table), canBtn, 1, 2, 7, 8, GTK_FILL, GTK_FILL, 0, 0);
  //gtk_table_attach(GTK_TABLE(table), actBtn, 2, 3, 7, 8, GTK_FILL, GTK_FILL, 0, 0);

  gtk_container_add(GTK_CONTAINER(window), table);
  // g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), G_OBJECT(window));
  g_signal_connect(G_OBJECT(canBtn), "clicked", G_CALLBACK(Click_Cancel), G_OBJECT(pTest));
  g_signal_connect(G_OBJECT(actBtn), "clicked", G_CALLBACK(Click_Commit), G_OBJECT(pTest));

  close(shm_fd);
  gtk_widget_show_all(window);

  // gtk_main();
}
// --------------------------------------------------------------------------------
// POPUP MENU STUFF 

static void test_build_popup_menu(TestPlugin* pTest) {
  pTest->popup_menu = gtk_menu_new();
  GtkWidget *mnu_icon[4];
  GtkWidget *item_auto = gtk_image_menu_item_new_with_label ("Auto");
  mnu_icon[0] = gtk_image_new_from_file("/usr/share/icons/hicolor/16x16/status/argonone-fan-auto.png");
  gtk_image_menu_item_set_image((GtkImageMenuItem*)(item_auto), mnu_icon[0]);
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_auto);
  gtk_image_menu_item_set_always_show_image ((GtkImageMenuItem*)(item_auto), TRUE);
  g_signal_connect(item_auto, "activate", G_CALLBACK(test_mnu_auto), pTest);
  GtkWidget *item_off = gtk_image_menu_item_new_with_label ("Off");
  mnu_icon[1] = gtk_image_new_from_file("/usr/share/icons/hicolor/16x16/status/argonone-fan-off.png");
  gtk_image_menu_item_set_image((GtkImageMenuItem*)(item_off), mnu_icon[1]);
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_off);
  g_signal_connect(item_off, "activate", G_CALLBACK(test_mnu_off), pTest);
  gtk_image_menu_item_set_always_show_image ((GtkImageMenuItem*)(item_off), TRUE);
  GtkWidget *item_man = gtk_image_menu_item_new_with_label ("Manual");
  mnu_icon[2] = gtk_image_new_from_file("/usr/share/icons/hicolor/16x16/status/argonone-fan-man.png");
  gtk_image_menu_item_set_image((GtkImageMenuItem*)(item_man), mnu_icon[2]);
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_man);
  g_signal_connect(item_man, "activate", G_CALLBACK(test_mnu_man), pTest);
  gtk_image_menu_item_set_always_show_image ((GtkImageMenuItem*)(item_man), TRUE);
  GtkWidget *item_cool = gtk_image_menu_item_new_with_label ("Cooldown");
  mnu_icon[3] = gtk_image_new_from_file("/usr/share/icons/hicolor/16x16/status/argonone-fan-cooldown.png");
  gtk_image_menu_item_set_image((GtkImageMenuItem*)item_cool, mnu_icon[3]);
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_cool);
  g_signal_connect(item_cool, "activate", G_CALLBACK(test_mnu_cool), pTest); 
  gtk_image_menu_item_set_always_show_image ((GtkImageMenuItem*)(item_cool), TRUE); 
// SEPERATOR - Fan Speed
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), gtk_separator_menu_item_new());
  GtkWidget *item_up = gtk_menu_item_new_with_label ("Fan speed up");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_up);
  g_signal_connect(item_up, "activate", G_CALLBACK(test_mnu_up), pTest);
  GtkWidget *item_down = gtk_menu_item_new_with_label ("Fan speed down");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_down);
  g_signal_connect(item_down, "activate", G_CALLBACK(test_mnu_down), pTest);
// SEPERATOR - About window
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), gtk_separator_menu_item_new());
  GtkWidget *item_about = gtk_menu_item_new_with_label ("About");
  gtk_menu_shell_append(GTK_MENU_SHELL(pTest->popup_menu), item_about);
  g_signal_connect(item_about, "activate", G_CALLBACK(test_mnu_about), pTest);
  
  gtk_widget_show_all(pTest->popup_menu);
}

static void test_popup_menu_set_position(GtkMenu *menu, gint *px, gint *py, gboolean *push_in, gpointer user_data) {
  TestPlugin *pTest = (TestPlugin *)user_data;
  lxpanel_plugin_popup_set_position_helper (pTest->panel, pTest->plugin, GTK_WIDGET(menu), px, py);
  *push_in = TRUE;
}

static gboolean test_button_press_event(GtkWidget *widget, GdkEventButton *event, LXPanel *panel) {
  TestPlugin *pTest = lxpanel_plugin_get_data(widget);
  
  if (event->button == 1) {
    // Left-click
    if (pTest->popup_menu == NULL) test_build_popup_menu(pTest);
    gtk_menu_popup(GTK_MENU(pTest->popup_menu), NULL, NULL, test_popup_menu_set_position, pTest, event->button, event->time);
    return TRUE;
  } else if (event->button == 3) {
    // Right-click
  }

  return FALSE;
}

static void test_destructor(gpointer user_data)
{
  TestPlugin *pTest = (TestPlugin *)user_data;
  
  if (pTest->popup_menu != NULL) gtk_widget_destroy(pTest->popup_menu);
  
  g_free(pTest);
}

GtkWidget *test_constructor(LXPanel *panel, config_setting_t *settings)
{
 // allocate our private structure instance
  TestPlugin *pTest = g_new0(TestPlugin, 1);
  GtkWidget *hbox;

  // Initialize things
  pTest->show_state = FALSE;
  pTest->show_speed = TRUE;
  pTest->show_temp = TRUE;
  pTest->cooldown_temp = 40;
  pTest->cooldown_fan = 10;
  pTest->manual_fan = 0;
  pTest->target_mode = -1;
  pTest->current_mode = -1;
  pTest->popup_menu = NULL;
  pTest->settings = settings;
  int val;
  if ( config_setting_lookup_int(pTest->settings, "showstate", &val) )    { pTest->show_state = val == 1 ? TRUE : FALSE; }
  if ( config_setting_lookup_int(pTest->settings, "showspeed", &val) )    { pTest->show_speed = val == 1 ? TRUE : FALSE; }
  if ( config_setting_lookup_int(pTest->settings, "showtemp", &val) )     { pTest->show_temp = val == 1 ? TRUE : FALSE; }
  if ( config_setting_lookup_int(pTest->settings, "cooldowntemp", &val) ) { pTest->cooldown_temp = val; }
  if ( config_setting_lookup_int(pTest->settings, "cooldownfan", &val) )  { pTest->cooldown_fan = val; }
  pTest->panel = panel;
  pTest->plugin = gtk_button_new();
  gtk_button_set_relief(GTK_BUTTON(pTest->plugin), GTK_RELIEF_NONE);
  g_signal_connect(pTest->plugin, "button-press-event", G_CALLBACK(test_button_press_event), pTest->panel);
  gtk_widget_add_events(pTest->plugin, GDK_BUTTON_PRESS_MASK);
  gtk_widget_set_tooltip_text(pTest->plugin, "ArgonOne fan");
  lxpanel_plugin_set_data (pTest->plugin, pTest, test_destructor);
#if GTK == 2
  hbox = gtk_hbox_new(FALSE, 2);
#elif GTK == 3
  hbox = gtk_box_new(FALSE, 2);
#else
#endif
  pTest->tray_icon = gtk_image_new();
  gtk_box_pack_start(GTK_BOX(hbox), pTest->tray_icon, TRUE, TRUE, 0);
  pTest->tray_label = gtk_label_new(NULL);
  gtk_box_pack_start(GTK_BOX(hbox), pTest->tray_label, TRUE, TRUE, 0);
  gtk_container_add (GTK_CONTAINER (pTest->plugin), hbox);
  gtk_widget_show_all(pTest->plugin);
  gchar *icon_name = "argonone-fan";
    lxpanel_plugin_set_taskbar_icon(
        pTest->panel, pTest->tray_icon,
        icon_name);
  g_timeout_add_seconds(INTV, &getstate, (gpointer)pTest);
  return pTest->plugin;
}
static gboolean test_apply_configuration(gpointer user_data)
{
    TestPlugin *pTest = lxpanel_plugin_get_data((GtkWidget *)user_data);
    if (pTest->cooldown_temp < 35 )pTest->cooldown_temp = 35;
    if (pTest->cooldown_temp > 75 )pTest->cooldown_temp = 75;
    if (pTest->cooldown_fan < 10 )pTest->cooldown_temp = 10;
    if (pTest->cooldown_temp > 100 )pTest->cooldown_temp = 100;

    config_group_set_int(pTest->settings, "showstate", (int)pTest->show_state);
    config_group_set_int(pTest->settings, "showspeed", (int)pTest->show_speed);
    config_group_set_int(pTest->settings, "showtemp", (int)pTest->show_temp);
    config_group_set_int(pTest->settings, "cooldowntemp", (int)pTest->cooldown_temp);
    config_group_set_int(pTest->settings, "cooldownfan", (int)pTest->cooldown_fan);

    return TRUE;
}
static void test_reconfigure(LXPanel *panel, GtkWidget *widget)
{
    TestPlugin *pTest = lxpanel_plugin_get_data(widget);
    if (pTest->cooldown_temp < 35 )pTest->cooldown_temp = 35;
    if (pTest->cooldown_temp > 75 )pTest->cooldown_temp = 75;
    if (pTest->cooldown_fan < 10 )pTest->cooldown_temp = 10;
    if (pTest->cooldown_temp > 100 )pTest->cooldown_temp = 100;
}
static GtkWidget *test_configure(LXPanel *panel, GtkWidget *widget)
{
    TestPlugin *pTest = lxpanel_plugin_get_data(widget);

    
    return lxpanel_generic_config_dlg(
      ("Argon One Fan Monitor"), panel,
      test_apply_configuration, widget,
      ("Show fan state [On/Off]"), &pTest->show_state, CONF_TYPE_BOOL,
      ("Show fan speed"), &pTest->show_speed, CONF_TYPE_BOOL,
      ("Show system temperature"), &pTest->show_temp, CONF_TYPE_BOOL,
      ("Cooldown target temperature"), &pTest->cooldown_temp, CONF_TYPE_INT,
      ("Cooldown fan speed"), &pTest->cooldown_fan, CONF_TYPE_INT,
      NULL
    );
}

FM_DEFINE_MODULE(lxpanel_gtk, test)


/* Plugin descriptor. */
LXPanelPluginInit fm_module_init_lxpanel_gtk = {
   .name = "ArgonOne",
   .description = "Argon One Fan Monitor",

   // assigning our functions to provided pointers.
   .new_instance = test_constructor,
   .config = test_configure,
   .reconfigure = test_reconfigure,
};
